name 'apache_tomcat_test'
version '0.0.1'
description 'Not a real cookbook. For testing purposes only!'

depends 'apache_tomcat'
depends 'java', '~> 1.31'
